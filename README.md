Project Overview:

This project focuses on deploying a sample game application on Amazon Elastic Kubernetes Service (EKS) with a Fargate profile. The application will be exposed to the outside world using Ingress along with the ALB Ingress Controller.




![2048-app](Screenshot_2023-09-08_161345.png)


For an in-depth exploration of the benefits of utilizing EKS and Fargate, refer to my article [here](https://medium.com/@dropamailtoshruti/streamlining-kubernetes-operations-with-eks-and-fargate-3c527cf53a27).

References:

GitHub Repository : https://github.com/iam-veeramalla/aws-devops-zero-to-hero.git
